﻿## Flappy Bird
## Membre
* Guillaume VANDECASTEELE

**Le fichier executable est trouvable dans le dossier ./Building/

## Type de jeu
* Un flappy Fird classique

## Instructions :
* Lorsque vous aurez accès à la page d'accueil, cliquez sur le bouton pour accéder au jeu
* Pour faire voler l'oiseau, appuyez sur la barre d'espace
* Lorsque vous perdez, appuyez sur la barre d'espace pour rejouer

## Difficultés rencontrées
* Le background ne s'arrêtais pas lorsque l'oiseau mourrais, c'est désormais réglé grâce au paramètre Vector correspondant au défilement étant mis à 0
* Lorsque le background se recréé vers la droite, il n'était pas parfaitement recréé et affichait une barre noire au milieu de l'écran. Maintenant, lorsque le background est recréé, il est recréé avec un peu plus de largeur
* le score n'était pas remis à jour lorsque l'on rejouait. J'ai rajouté une fonction dans le GameState pour réinitialiser le score du joueur

## Difficultés non résolues 
- Portage sur téléphone non mis
- Écran non cliquable
- Génération aléatoire de la position des tuyaux ne prends pas en compte le sol

## Plateforme développée 
* Windows

