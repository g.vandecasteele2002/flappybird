using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class wait2Sec : MonoBehaviour
{
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        Invoke("GoScene2",5.0f);
    }

    void GoScene2(){ 
        SceneManager.LoadScene("scene2-Menu");  
    }
    
}
