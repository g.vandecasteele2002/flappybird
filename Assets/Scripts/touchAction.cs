using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class touchAction : MonoBehaviour
{
    // Start is called before the first frame update

    private AudioSource[] audioData;
    public Vector2 velocity;
    void Start()
    {
        audioData = GetComponents<AudioSource>();
        velocity = new Vector2(0, 9);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space")){
            GetComponent<Rigidbody2D>().velocity = velocity;
            audioData[0].Play(0);
        }

        if (Input.touchCount > 0){
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    GetComponent<Rigidbody2D>().velocity = velocity;
                    audioData[0].Play(0);
                    break;
            }
        }
    }
}
