using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameState : MonoBehaviour
{
    public static GameState Instance;
    private int scorePlayer = 0;
    private int maxScore = 0;
    void Start () {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad (Instance.gameObject);
        }
        else if (this != Instance) {
            Debug.Log ("Detruit");
            Destroy (this.gameObject);
        }
        GameObject.FindWithTag("scoreLabel").GetComponent<TextMeshProUGUI>().text=""+scorePlayer;
    }

    private void Update()
    {
        FixedUpdate();
    }

    public void AddScorePlayer(int toAdd) {
        scorePlayer += toAdd;
    }

    private void FixedUpdate(){
        // check only in the scene 3
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "scene3-Game"){
            GameObject.FindWithTag("scoreLabel").GetComponent<TextMeshProUGUI>().text=""+scorePlayer;
        }
    }

    public int GetScorePlayer(){
		return scorePlayer;	
	}
    
    public int GetMaxScore()
    {
        return maxScore;
    }
    
    public void ResetScorePlayer(){
        scorePlayer = 0;
    }

}
