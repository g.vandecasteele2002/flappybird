using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class collideManagementBird : MonoBehaviour
{
    private Vector3 leftBottomCameraBorder;
    private Vector3 rightBottomCameraBorder; 
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;
    private Vector3 size;
    private float topGroundPosition;
    private bool killed = false;
    private AudioSource[] audioData;
    
    // Start is called before the first frame update
    void Start()
    {
        audioData = GetComponents<AudioSource>();
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        topGroundPosition = GameObject.FindGameObjectsWithTag("ground")[0].transform.localScale.y;
    }

    // Update is called once per frame
    void Update()
    {
        size.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        //Ground border
        
        if (transform.position.y < leftBottomCameraBorder.y + topGroundPosition + (size.y / 2)){
            if (!killed){
                EndAction();
                StopCoroutine("RotateMe");
            }
            gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;
            Invoke("GoScene4", 1.5f);
            
        }

        //Top border
        if (transform.position.y > leftTopCameraBorder.y - (size.y / 2)) {
            EndAction();
            audioData[3].PlayDelayed(audioData[2].clip.length);
        }
    }

    IEnumerator RotateMe(Vector3 byAngles, float inTime)
    {
        var fromAngle = transform.rotation;
        var toAngle = Quaternion.Euler(transform.eulerAngles + byAngles);
        for (var t = 0f; t < 1; t += Time.deltaTime / inTime)
        {
            transform.rotation = Quaternion.Slerp(fromAngle, toAngle, t);
            yield return null;
        }
    }

    void OnTriggerEnter2D(Collider2D collider){
        if ((collider.name == "upPipe" || collider.name == "downPipe") && !killed){
            EndAction();
            audioData[3].PlayDelayed(audioData[2].clip.length);
        }
    }

    void OnTriggerExit2D(Collider2D collider){
        if (collider.name == "box" && !killed){
            GameState.Instance.AddScorePlayer(1);
            audioData[1].Play(0);
        }
    }

    void EndAction(){
        killed = true;
        gameObject.GetComponent<Animator>().enabled = false;
        StartCoroutine(RotateMe(Vector3.back * 90, 0.8f));
        GameObject[] pipes = GameObject.FindGameObjectsWithTag("pipe");
        foreach (GameObject pipe in pipes){
            pipe.GetComponent<movePipes>().stopMove();
        }
        GameObject[] backGrounds = GameObject.FindGameObjectsWithTag("background");
        foreach (GameObject backGround in backGrounds){
            backGround.GetComponent<moveBk>().stopMove();
        }
        GameObject[] grounds = GameObject.FindGameObjectsWithTag("ground");
        foreach (GameObject ground in grounds){
            ground.GetComponent<moveBk>().stopMove();
        }
        audioData[2].Play(0);
        Destroy(gameObject.GetComponent<touchAction>());
    }
    
    void GoScene4(){
        SceneManager.LoadScene("scene4-End");
    }
}
