using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class clickButton : MonoBehaviour
{
    AudioSource audioData;
    
    void Start(){
        audioData = GetComponent<AudioSource>();
    }
    public void onClick(){
        audioData.Play(0);
        Invoke("GoScene3", audioData.clip.length);
    }

    void GoScene3(){ 
        SceneManager.LoadScene("scene3-Game");  
    }
}
