using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class restartAction : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space")){
            Invoke("GoScene3",0.0f);
        }

        if (Input.touchCount > 0){
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    Invoke("GoScene3",0.0f);
                    break;
            }
        }
        
    }
    
    public void OnMouseDown(){
        Invoke("GoScene3",0.0f);
    }

    void GoScene3()
    {
        GameState.Instance.ResetScorePlayer();
        SceneManager.LoadScene("scene3-Game"); 
    }
}
