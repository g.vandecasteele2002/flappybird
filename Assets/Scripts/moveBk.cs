using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBk : MonoBehaviour
{
    private Vector3 leftBottomCameraBorder;
    private Vector3 rightBottomCameraBorder; 
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;

    public Vector2 movement;
    public float positionRestartX;

    private Vector3 siz;
    // Start is called before the first frame update
    void Start()
    {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        positionRestartX = GameObject.Find("backGround3").transform.position.x;
        movement = new Vector2(-2.5f,0);
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = movement;
        siz.x = gameObject.GetComponentInChildren<SpriteRenderer>().bounds.size.x;
        siz.y = gameObject.GetComponentInChildren<SpriteRenderer>().bounds.size.y;

        // If the backgound exits the screen
        // Set the X position with the original backGround3	X position

        if (transform.position.x < leftBottomCameraBorder.x - (siz.x / 2))
        {
            transform.position = new Vector3(positionRestartX-1, transform.position.y, transform.position.z);
        }
    }
    
    public void stopMove(){
        movement = new Vector2(0,0);
    }
}
