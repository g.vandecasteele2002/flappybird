using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePipes : MonoBehaviour
{  
    private Vector3 leftBottomCameraBorder;
    private Vector3 rightBottomCameraBorder; 
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;

    public Vector2 movement;
    public GameObject pipe1Up;
    public GameObject pipe1Down;
    public GameObject box;
    public GameObject ground;

    private Transform pipe1UpOriginalTransform;
    private Transform pipe1DownOriginalTransform;
    private Transform boxOriginalTransform;

    private Vector3 siz;

    // Start is called before the first frame update
    void Start()
    {
        pipe1Down = transform.Find("downPipe").gameObject;
        pipe1Up = transform.Find("upPipe").gameObject;
        box = transform.Find("box").gameObject;
        ground = GameObject.FindGameObjectWithTag("ground");

        pipe1DownOriginalTransform = pipe1Down.transform;
        pipe1UpOriginalTransform = pipe1Up.transform;
        boxOriginalTransform = box.transform;

        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

        movement = new Vector2(-2.5f,0);
    }

    // Update is called once per frame
    void Update() 
    {  
        pipe1Up.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe haut 
        pipe1Down.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe bas 
        box.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement de la box
        siz.x = pipe1Up.GetComponent<SpriteRenderer>().bounds.size.x; // Récuperation de la taille d’un pipe  
        siz.y = pipe1Up.GetComponent<SpriteRenderer>().bounds.size.y; // Suffisant car ils ont la même taille
    // Le pipe est sorti de l’écran ? Si oui appel de la méthode moveToRightPipe
        if (pipe1Up.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)) moveToRightPipe(); 
    } 

    void moveToRightPipe(){ 
        float randomY = Random.Range (1.0f,4f) - 2; // Tirage aléatoire d’un décalage en Y 
        float posX = rightBottomCameraBorder.x + (siz.x / 2); // Calcul du X du bord droite de l’écran
        // Calcul du nouvel Y en reprenant la position Y d’origine du pipe, ici le downPipe1 
        float posY = pipe1UpOriginalTransform.position.y + randomY; 
        // Création du vector3 contenant la nouvelle position 
        Vector3 tmpPos = new Vector3 (posX, posY, pipe1Up.transform.position.z); 
        // Affectation de cette nouvelle position au transform du gameObject
        pipe1Up.transform.position = tmpPos; 

        // Idem pour le second pipe 
        posY = pipe1DownOriginalTransform.position.y + randomY; 
        tmpPos = new Vector3 (posX, posY, pipe1Down.transform.position.z); 
        pipe1Down.transform.position = tmpPos;

        // Idem pour la box
        posY = boxOriginalTransform.position.y + randomY; 
        tmpPos = new Vector3 (posX, posY, box.transform.position.z); 
        box.transform.position = tmpPos;
    }
    
    
    public void stopMove(){
        movement = new Vector2(0,0);
    }
}
